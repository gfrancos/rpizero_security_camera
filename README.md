# RPIZero_Security_Camera

Proyecto de streaming local con detector de movimiento y notificaciones a telegram

## Main Libraries
* Opencv
* Flask

## How to Run 
1. Install **Requirements**:

    ```
    pip install -r requirements.txt
    ```

2. Run the app:

    ```
    python server.py
