import json
import scipy.misc
from datetime import datetime
import cv2, time
import telegram_send 
from datetime import datetime, timedelta
import threading
first_frame=None
last_sent_time=None
SENSOR_INTERVAL = timedelta(minutes=2)

class VideoCamera(object):
 
    def __init__(self):
        # Open a camera
        self.cap = cv2.VideoCapture(0)
        self.out = None

    
    def __del__(self):
        self.cap.release()
    
    def get_frame(self):
        global first_frame
        global mov
        global last_sent_time
        while True:
            ret, frame = self.cap.read()
            timestamp = datetime.now()
            text = "No hay Movimiento.."
            
            frame = cv2.resize(frame, None, fx=0.6, fy=0.6, interpolation=cv2.INTER_AREA)        
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
            gray = cv2.GaussianBlur(gray,(21,21),0)
    
            if  first_frame is None :
                first_frame = gray
                continue
    
            delta_frame = cv2.absdiff(first_frame,gray)
            thresh_delta = cv2.threshold(delta_frame,30,255,cv2.THRESH_BINARY)[1]
            thresh_delta = cv2.dilate(thresh_delta, None, iterations=3)
    
            (cnts,_) = cv2.findContours(thresh_delta.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                        
            for contour in cnts:
                if cv2.contourArea(contour) < 10000:
                    continue
                
                (x, y, w, h) = cv2.boundingRect(contour)
                cv2.rectangle(frame, (x,y), (x+w,y+h), (0, 255, 0), 3)
                text = "Movimiento Detectado!"

            # draw the text and timestamp on the frame
            #ts = timestamp.strftime("%A %d %B %Y %I:%M:%S%p") + str(timedelta(hours=6))
            ts = datetime.today() + timedelta(hours=6)
            ts = ts.strftime("%A %d %B %Y %I:%M:%S%p")
            cv2.putText(frame, "Status: {}".format(text), (10, 20),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
            cv2.putText(frame, ts, (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX,
                0.35, (0, 0, 255), 1)

            # check to see if the room is occupied
            if text == "Movimiento Detectado!":
                # check to see if enough time has passed between uploads
                if not last_sent_time or (datetime.now() - last_sent_time) > SENSOR_INTERVAL:
                    last_sent_time = datetime.now()
                    print("[INFO] Movimiento Detectado!")
                    #telegram_send.send (messages = ["Movimiento detectado"])
                    scipy.misc.imsave('/home/pi/Motion-Detector-master/saved_imgs/outfile.jpg', frame)
                    with open("/home/pi/Motion-Detector-master//saved_imgs/outfile.jpg", "rb") as f:
                        telegram_send.send(images=[f],messages = ["Movimiento detectado"])
                
            if ret:
                ret, jpeg = cv2.imencode('.jpg', frame)
                return jpeg.tobytes()
      
            else:
                return None

